from django.db import models


class Entry(models.Model):
    content = models.TextField()
    name = models.TextField()
    type = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.type}: {self.name} ({self.content})"


class Queue(models.Model):
    name = models.TextField()
    entries = models.ManyToManyField(Entry, through="QueueEntries")

    def __str__(self):
        return f"{self.name} ({self.entries} Entries)"


class QueueEntries(models.Model):
    queue = models.ForeignKey(Queue, on_delete=models.CASCADE)
    entry = models.ForeignKey(Entry, on_delete=models.CASCADE)
    order = models.IntegerField()

    def __str__(self):
        return f"{self.queue.name} <-> {self.entry.name} ({self.order}"


class Output(models.Model):
    name = models.TextField()
    comment = models.TextField()
    queue = models.ForeignKey(Queue, on_delete=models.CASCADE)
