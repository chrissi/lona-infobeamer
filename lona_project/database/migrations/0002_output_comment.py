# Generated by Django 4.1 on 2022-08-30 12:30

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("database", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="output",
            name="comment",
            field=models.TextField(default=""),
            preserve_default=False,
        ),
    ]
