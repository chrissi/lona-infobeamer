from aiohttp_wsgi import WSGIHandler
from lona.routing import Route

from lona_project._django.wsgi import application

wsgi_handler = WSGIHandler(application)

routes = [
    Route("/", "views/home.py::HomeView"),
    Route("/backend(/)", "views/backend.py::BackendView"),
    Route("/backend/upload(/)", "views/backend.py::UploadView", interactive=False),
    Route("/admin<path:.*>", wsgi_handler, http_pass_through=True),
    Route("/<output>(/)", "views/home.py::HomeView"),
]
