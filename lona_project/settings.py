MAX_WORKER_THREADS = 4
MAX_STATIC_THREADS = 4
MAX_RUNTIME_THREADS = 6

ROUTING_TABLE = "routes.py::routes"

STATIC_DIRS = [
    "static",
]

TEMPLATE_DIRS = [
    "templates",
]

UPLOAD_DIR = "lona_project/static/upload/"
DELAY = 10
MULTI = 20

AIOHTTP_CLIENT_MAX_SIZE = 10 * 1024**2
