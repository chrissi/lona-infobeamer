from lona_project.database.models import (
    Queue,
    Entry,
    Output,
    QueueEntries,
)
from django.contrib import admin

admin.site.register(Queue)
admin.site.register(Entry)
admin.site.register(Output)
admin.site.register(QueueEntries)
