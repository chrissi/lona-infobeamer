import os
import time

from django.db.models import Max
from lona.html import (
    H1,
    H2,
    P,
    HTML,
    Button,
    Form,
    Node,
    Submit,
    A,
    TextInput,
    Div,
    I,
    Table,
    Tr,
    Td,
    Th,
    Img,
)
from lona.view import LonaView

from lona_project.database.models import (
    Queue,
    Entry,
    Output,
    QueueEntries,
)
from lona_project.tabs import Tabs, TabPane

MARKER_OUTPUT = "📺️ "
MARKER_QUEUE = "🔢 "
MARKER_CONTENT = "🖼️ "


class File(Node):
    TAG_NAME = "input"
    SELF_CLOSING_TAG = True

    ATTRIBUTES = {
        "type": "file",
    }


class UploadView(LonaView):
    def handle_request(self, request):
        if request.method == "POST":
            content = request.POST["img_file"].file.read()
            filename = request.POST["img_file"].filename
            diskname = f"{str(time.time())}_{filename}"
            with open(os.path.join(self.server.settings.UPLOAD_DIR, diskname), "bw+") as fh:
                fh.write(content)

            try:
                e = Entry.objects.get(name=filename)
                e.content = diskname
            except Entry.DoesNotExist:
                e = Entry.objects.create(
                    content=diskname,
                    name=filename,
                    type="img",
                )
            e.save()

            return {"http_redirect": "/backend"}

        html = HTML(
            H1("Upload Image"),
            P(
                "Select and upload your new image here. " "If a file with this name exists it will be replaced.",
                Form(
                    File(
                        _id="img_file",
                        name="img_file",
                    ),
                    Submit(
                        "Upload",
                    ),
                    action="/backend/upload",
                    method="post",
                    enctype="multipart/form-data",
                ),
            ),
        )
        return html


class BackendView(LonaView):
    def update_avaliable_content(self):
        self.available_content.clear()
        entries = Entry.objects.all()
        header = Tr(
            Th("Add to queue"),
            Th("Name"),
            Th("Preview"),
            Th("Delete"),
        )
        self.available_content.append(header)
        for entry in entries:
            delete = Button(
                "Löschen",
                id="entry_delete",
            )
            delete.entry = entry

            queue_buttons = []
            for queue in Queue.objects.all():
                b = Button(
                    MARKER_QUEUE + queue.name,
                    id="entry_to_queue",
                )
                b.entry = entry
                b.queue = queue
                queue_buttons.append(b)

            if entry.type == "img":
                url = f"static/upload/{entry.content}"
                preview = Img(src=url, _class="preview")
            elif entry.type == "iframe":
                url = entry.content
                preview = ""
            else:
                raise RuntimeError(f"Unknown Entry type {entry.type} in {entry}.")
            tr = Tr(
                Td(*queue_buttons),
                Td(
                    A(
                        entry.name,
                        href=url,
                        interactive=False,
                        target="_blank",
                    ),
                ),
                Td(preview),
                Td(delete),
            )
            self.available_content.append(tr)

    def update_queues(self):
        self.queues.clear()
        for queue in Queue.objects.all():
            if queue.name == "Default":
                title = Tr(Td(MARKER_QUEUE + queue.name, colspan=4))
            else:
                delete_queue = Button(
                    "Löschen",
                    id="queue_delete",
                )
                title = Tr(
                    Td(MARKER_QUEUE + queue.name, colspan=4),
                    Td(delete_queue),
                )
                delete_queue.queue = queue
            self.queues.append(title)

            for queue_entry in QueueEntries.objects.filter(queue=queue).order_by("order"):
                entry = queue_entry.entry
                if entry.type == "img":
                    url = f"static/upload/{entry.content}"
                elif entry.type == "iframe":
                    url = entry.content
                else:
                    raise RuntimeError(f"Unknown Entry type {entry.type} in {entry}.")
                delete = Button(
                    "Löschen",
                    id="queue_entry_delete",
                )
                delete.queue_entry = queue_entry
                delete.queue = queue

                up = Button("↑", id="queue_entry_up")
                up.queue_entry = queue_entry
                up.queue = queue
                down = Button("↓", id="queue_entry_down")
                down.queue_entry = queue_entry
                down.queue = queue

                e = Tr(
                    Td(),
                    Td(
                        MARKER_CONTENT,
                        A(
                            entry.name,
                            href=url,
                            interactive=False,
                            target="_blank",
                        ),
                    ),
                    Td(up, down),
                    Td(delete),
                )
                self.queues.append(e)

    def store_comment(self, input_event):
        input_event.node.output.comment = input_event.node.value
        input_event.node.output.save()

    def update_outputs(self):
        self.outputs.clear()
        header = Tr(
            Th("Name"),
            Th("Comment"),
            Th("Playing"),
            Th("Play instead"),
        )
        self.outputs.append(header)
        for output in Output.objects.all():
            queues = []
            for queue in Queue.objects.all():
                if queue != output.queue:
                    b = Button(
                        MARKER_QUEUE + queue.name,
                        _id="output_activate_queue",
                    )
                    b.queue = queue
                    b.output = output
                    queues.append(b)
            if len(queues) == 0:
                queues.append(I("No other queue available"))
            tr = Tr(
                Td(MARKER_OUTPUT + output.name),
                Td(comment := TextInput(output.comment, bubble_up=True, handle_change=self.store_comment)),
                Td(MARKER_QUEUE + output.queue.name),
                Td(*queues),
            )
            comment.output = output
            self.outputs.append(tr)

    def handle_request(self, request):
        self.set_title("Infobeamer Backend")
        # Ensure the default queue is available
        try:
            Queue.objects.get(name="Default")
        except Queue.DoesNotExist:
            q = Queue(name="Default")
            q.save()

        self.available_content = Table()
        self.queues = Table()
        self.outputs = Table()

        iframe_url = TextInput(placeholder="https://hackenopenair.de")
        queue_name = TextInput(placeholder="Descriptive Name")

        html = HTML(
            H1("Lona Infobeamer Backend"),
            Tabs(
                TabPane(
                    MARKER_CONTENT + "Content",
                    Div(
                        H2("Upload Image"),
                        P(
                            A("Go here for upload", href="/backend/upload"),
                        ),
                        H2("Add an URL as iframe"),
                        P(
                            iframe_url,
                            Button("Hinzufügen", id="iframe_add"),
                        ),
                        H2("Available Content"),
                        P(
                            self.available_content,
                        ),
                    ),
                ),
                TabPane(
                    MARKER_QUEUE + "Queues",
                    Div(
                        H2("Create new queue"),
                        P(
                            queue_name,
                            Button("Create", id="queue_add"),
                        ),
                        H2("Available queues"),
                        P(
                            self.queues,
                        ),
                    ),
                ),
                TabPane(
                    MARKER_OUTPUT + "Outputs",
                    Div(
                        H2("Registered Outputs"),
                        P(
                            self.outputs,
                        ),
                    ),
                ),
            ),
        )

        self.update_avaliable_content()
        self.update_queues()
        self.update_outputs()

        while True:
            event = self.await_input_event(html=html)
            if "iframe_add" in event.node.id_list:
                entry = Entry.objects.create(
                    content=iframe_url.value,
                    name=iframe_url.value,
                    type="iframe",
                )
                entry.save()
                self.update_avaliable_content()
                iframe_url.value = ""
            elif "entry_delete" in event.node.id_list:
                if event.node.entry.type == "img":
                    os.remove(os.path.join(self.server.settings.UPLOAD_DIR, event.node.entry.content))
                queue_entries = QueueEntries.objects.filter(entry=event.node.entry)
                for queue_entry in queue_entries:
                    queue_entry.delete()
                event.node.entry.delete()
                self.update_avaliable_content()
                self.update_queues()
            elif "queue_add" in event.node.id_list:
                if len(queue_name.value) > 0 and queue_name.value != "Default":
                    queue = Queue.objects.create(name=queue_name.value)
                    queue.save()
                    queue_name.value = ""
                    self.update_queues()
                    self.update_avaliable_content()
                    self.update_outputs()
            elif "queue_delete" in event.node.id_list:
                event.node.queue.delete()
                self.update_queues()
                self.update_avaliable_content()
                self.update_outputs()
            elif "entry_to_queue" in event.node.id_list:
                queue = event.node.queue
                entry = event.node.entry
                order = QueueEntries.objects.filter(queue=queue).aggregate(Max("order"))
                order = order["order__max"] if order["order__max"] else 0
                qe = QueueEntries(queue=queue, entry=entry, order=order + 1)
                qe.save()
                self.update_queues()
            elif "queue_entry_delete" in event.node.id_list:
                queue = event.node.queue
                queue_entry = event.node.queue_entry

                entries = QueueEntries.objects.filter(queue=queue).order_by("order")
                hit = True
                for qe in entries:
                    if hit:
                        qe.order -= 1
                        qe.save()
                    if qe == queue_entry:
                        hit = True
                event.node.queue_entry.delete()
                self.update_queues()
            elif "queue_entry_up" in event.node.id_list:
                queue = event.node.queue
                queue_entry = event.node.queue_entry

                entries = QueueEntries.objects.filter(queue=queue).order_by("order")
                prev = None
                for qe in entries:
                    if qe == queue_entry:
                        if prev:
                            prev.order += 1
                            qe.order -= 1
                            prev.save()
                            qe.save()
                        break
                    prev = qe
                self.update_queues()
            elif "queue_entry_down" in event.node.id_list:
                queue = event.node.queue
                queue_entry = event.node.queue_entry

                entries = QueueEntries.objects.filter(queue=queue).order_by("order")
                found = False
                for idx, qe in enumerate(entries):
                    if not found:
                        if qe == queue_entry and idx != len(entries) - 1:
                            qe.order += 1
                            qe.save()
                            found = True
                    else:
                        qe.order -= 1
                        qe.save()
                        break
                self.update_queues()

            elif "output_activate_queue" in event.node.id_list:
                event.node.output.queue = event.node.queue
                event.node.output.save()
                self.update_outputs()
            else:
                print("Unhandled Event")
                print(event)
