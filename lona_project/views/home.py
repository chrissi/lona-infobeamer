import logging

from lona.html import HTML, Img, Div, Node, I
from lona.view import LonaView

from lona_project.database.models import (
    Queue,
    Output,
    QueueEntries,
)


class Iframe(Node):
    TAG_NAME = "iframe"


def _get_content_node(entry):
    if entry.type == "img":
        return Img(
            src=f"/static/upload/{entry.content}",
            style={"width": "100%"},
        )
    elif entry.type == "iframe":
        return Iframe(
            src=entry.content,
            style={"width": "100%", "height": "100%"},
        )


class HomeView(LonaView):
    def handle_request(self, request):
        self.set_title("Infobeamer")

        # get name of output requesting this view
        if "output" in request.match_info:
            output_name = request.match_info["output"]
        else:
            output_name = "Default"

        self.logger = logging.getLogger(f'home-view "{output_name}" (id: {id(self)})')
        self.logger.info("output connected")

        # get a queue for this output
        try:
            # works if this output_name is already known
            Output.objects.get(name=output_name)
        except Output.DoesNotExist:
            # if it is the first time we encounter this output_name we will create it in the db and
            # assign the "Default" -queue
            queue = Queue.objects.get(name="Default")
            output = Output(name=output_name, queue=queue)
            output.save()
            self.logger.info("This is a new output name. Added to DB.")

        div_content = Div(style={"width": "100%", "height": "100vh"})
        html = HTML(
            div_content,
        )

        def _get_queue():
            outp = Output.objects.get(name=output_name)
            return list(QueueEntries.objects.filter(queue=outp.queue).order_by("order"))

        def _sleep():
            self.sleep(getattr(self.server.settings, "DELAY", 10))

        while True:
            queue = _get_queue()
            div_content.nodes.clear()
            div_content.set_text("")
            self.logger.debug("Fond %i items in our queue", len(queue))
            if len(queue) == 0:
                self.logger.debug("Showing idle screen")
                div_content.append(Div("Nothing to display :-( ", I(f"(This is output '{output_name}')")))
                self.show(html)
                _sleep()
            elif len(queue) == 1:
                self.logger.debug("Entering single slide mode")
                # with only one entry in the queue, we will keep it displayed for up to
                # MULTI*DELAY if the filename / url does not change.
                # after that time, we will trigger a reload
                entry = queue[0].entry
                content = entry.content
                self.logger.info("Showing %s", entry)
                div_content.nodes.append(_get_content_node(entry))
                self.show(html)
                for _ in range(getattr(self.server.settings, "MULTI", 20)):
                    _sleep()
                    queue = _get_queue()
                    if len(queue) != 1:
                        self.logger.debug("Queue len changed. Ending singe slide mode.")
                        break
                    entry = queue[0].entry
                    if entry.content != content:
                        self.logger.debug("Content in queue changed. Ending singe slide mode.")
                        break
            elif len(queue) >= 2:
                self.logger.debug("Entering multi slide mode")
                div_content.nodes.clear()
                div_content.set_text("")

                # with multiple entries in the queue, we will always display the current
                # element and have one element in a (hidden) next object.
                # if our queue is empty, we will load another queue.
                front_entry = queue.pop(0).entry
                front = _get_content_node(front_entry)
                self.logger.info("Showing %s", front_entry)
                div_content.nodes.append(front)

                back_entry = queue.pop(0).entry
                back = _get_content_node(back_entry)
                self.logger.info("Background loading %s", back_entry)
                back.hide()
                div_content.nodes.append(back)

                while True:
                    self.show(html)
                    _sleep()
                    if not len(queue):
                        self.logger.debug("Multi-slide queue empty. Reloading...")
                        queue = _get_queue()
                        if not len(queue) >= 2:
                            self.logger.debug("New queue is not long enough. Ending multi slide mode.")
                            break
                    old = front
                    front = back
                    front.show()
                    div_content.nodes.remove(old)
                    self.logger.debug("Showing background-loaded slide.")
                    back_entry = queue.pop(0).entry
                    back = _get_content_node(back_entry)
                    self.logger.info("Background loading %s", back_entry)
                    back.hide()
                    div_content.nodes.append(back)
