Lona Infobeamer
===============

This is a basic implementation of a digital signage system build around a
python based server and nodes running a web-browser.

Setting it up
-------------

You will need ``python3`` with ``python3-virtualenv`` and ``build-essential``
to set this up.

Then run:

```
make clean
make db
make server
```

Given you see no exeption during startup you can now surf to

* ``http://localhost:8080/backend``: For setup and control
* ``http://localhost:8080/``: For your signage output
